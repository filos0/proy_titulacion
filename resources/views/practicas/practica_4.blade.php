@extends('layouts.blog')

@section('content')

    <div class="pt-15">
        <div class="panel panel-default">
            <div class="panel-heading" style="background-color: white;border: white 1px solid">
                <div class="row">
                    <div class="text-center">
                        <span style="color: #007D3C;font-weight: bold;font-size: 24px">{{$practica->nombre}}</span>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <form action="{{url('/Practica/Finalizar')}}" method="post" id="form_practica">
                    {{ csrf_field() }}
                    <input type="hidden" name="id_practica" value="{{$practica->id_pratica}}">
                    <input type="hidden" name="num_comandos">
                    <input type="hidden" name="segundos">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="text-justify">
                                <div>
                                    <span style="font-weight: bold;font-size: 20px;color: black">Descripción de la Práctica </span>
                                    <br>
                                    <span style="font-size: 18px">
                                    {{$practica->descripcion}}
                                </span>
                                    <br>
                                    <br>
                                </div>
                                <div>
                                    <span style="font-weight: bold;font-size: 20px;color: black">Comandos a utilizar</span>
                                    <br>
                                    @foreach($practica->comandos as $comando)
                                        <a style="font-size: 18px;margin-left: 10px;color: #007D3C;font-weight: bold"
                                           target="_blank"
                                           href="{{url('comando/'.$comando->descripcion->nombre)}}">{{$comando->descripcion->nombre}}</a>
                                        <br>
                                    @endforeach
                                    <br>
                                </div>
                                <div>
                                    <span style="font-weight: bold;font-size: 20px;color: black">Objetivos </span>
                                    <br>
                                    <div class="table-responsive">
                                        <table class="table ">
                                            <tbody>
                                            <tr style="width: 100%;">
                                                <td style="width: 90%;border: none;">
                                                    <span style="font-size: 18px">Enlistar Grupos</span><br>
                                                    <div style="color: #007D3C;font-weight: bold;">
                                                        <span style="font-size: 18px">Ya que no existe un comando que enliste a los grupos </span><br>
                                                        <span style="font-size: 18px">Se modifico el comando ls para poder enlistarlos</span>
                                                    </div>
                                                </td>
                                                <td style="width: 10%;border: none;">
                                                <span style="font-size: 18px;font-weight: bold" id="valida_1"
                                                      class="txt-danger ">X</span>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%; ">
                                                <td style="width: 90%;border: none">
                                                    <span style="font-size: 18px">Agregar un nuevo grupo con nombre "grupo_impresoras"</span>
                                                </td>
                                                <td style="width: 10%;border: none">
                                                <span style="font-size: 18px;font-weight: bold" id="valida_2"
                                                      class="txt-danger  ">X</span>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%; ">
                                                <td style="width: 90%;border: none">
                                                    <span style="font-size: 18px">Agregar un nuevo grupo con nombre "grupo_admin"</span>
                                                </td>
                                                <td style="width: 10%;border: none">
                                                <span style="font-size: 18px;font-weight: bold" id="valida_3"
                                                      class="txt-danger  ">X</span>
                                                </td>
                                            </tr>
                                            <tr style="width: 100%; ">
                                                <td style="width: 90%;border: none">
                                                    <span style="font-size: 18px">Eliminar el usuario con nombre "grupo_backup"</span>
                                                </td>
                                                <td style="width: 10%;border: none">
                                                <span style="font-size: 18px;font-weight: bold" id="valida_4"
                                                      class="txt-danger  ">X</span>
                                                </td>
                                            </tr>
                                            </tbody>

                                        </table>
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="table-responsive" id="div_modelo">
                                <div id="term_demo"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="text-center">
                                <span style="font-weight: bold"
                                      class="text-center txt-dark font-22">Tiempo Transcurrido</span><br>
                                <div class="text-center txt-dark font-20">
                                    <span id="minutes"></span>:<span id="seconds"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <button disabled class="btn btn-block btn-danger " style="font-weight: bold"
                                    id="btn_chido">

                                <span class="text-center ">Finalizar Práctica</span>
                                <br>
                                <i class="fa fa-chevron-circle-right"></i>
                            </button>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="text-center">
                                <span style="font-weight: bold"
                                      class="text-center txt-dark font-22">Comandos Utilizados</span><br>
                                <div class="text-center txt-dark font-20">
                                    <span id="intentos"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            var intentos = 0;
            var seg_totales = 0;

            var valida_1 = $('#valida_1');
            var valida_2 = $('#valida_2');
            var valida_3 = $('#valida_3');
            var valida_4 = $('#valida_4');
            var btn_chido = $('#btn_chido');
            $('#intentos').text(intentos);


            $(function ($,) {
                setInterval(function () {
                    if (valida_1.text() === "X" || valida_2.text() === "X" || valida_3.text() === "X" || valida_4.text() === "X")
                        btn_chido.attr("disabled", true).removeClass().addClass("btn btn-block  btn-danger")

                    else
                        btn_chido.attr("disabled", false).removeClass().addClass("btn btn-block  btn-success")

                }, 1000);
            });


            $(function ($,) {
                var sec = 0;

                function pad(val) {
                    return val > 9 ? val : "0" + val;
                }

                setInterval(function () {
                    $("#seconds").html(pad(++sec % 60));
                    $("#minutes").html(pad(parseInt(sec / 60, 10)));
                    seg_totales = seg_totales + 1;
                    $("[name = segundos]").val(seg_totales);
                }, 1000);
            });

            $(function ($,) {
                var comandos = ["groupadd", "groupdel", "ls"];
                var usuarios = ["grupo_data","grupo_backup"];
                $('#term_demo').terminal(function (command, term) {
                    function aumento() {
                        intentos = intentos + 1;
                        $('#intentos').text(intentos);
                        $("[name = num_comandos]").val(intentos);
                    }

                    function existe(arr, element) {
                        return arr.includes(element);
                    }

                    if (command === "")
                        return false;

                    aumento();
                    var todo = command.match('(\\w+) ?(-\\w*)? ?(\\w*)');
                    if (todo === null) {
                        term.echo("¡Syntaxis Erronea ! ¡ KERNEL PANIC !");
                        return false;
                    }
                    var comando = todo[1];
                    var bandera = todo[2];
                    var usuario = todo[3];

                    if (!existe(comandos, comando)) {
                        term.echo("Comando no permitido o erroneo");
                        return false;
                    }

                    if ( bandera !== undefined) {
                        term.echo("Las opciones del comando "+comando+"  no estan permitidos en esta práctica");
                        return false;
                    }

                    if (command === "ls") {
                        term.echo(' ');
                        usuarios.forEach(function (x) {
                            term.echo(x);
                        });
                        term.echo(' ');
                        valida_1.removeClass().text("✓").addClass("txt-success")
                        return false;
                    }

                    if (comando === "groupadd"){
                        if (existe(usuarios, usuario)) {
                            term.echo("Error !! el grupo "+ usuario+ " ya existe");
                            return false;
                        }
                        else if (usuario === "grupo_admin") {
                            term.echo(' ');
                            term.echo('Se creó el usuario "grupo_admin"');
                            term.echo(' ');
                            usuarios.push(usuario);
                            valida_2.removeClass().text("✓").addClass("txt-success")
                            return false;
                        }
                        else if (usuario === "grupo_impresoras") {
                            term.echo(' ');
                            term.echo('Se creó el usuario "grupo_impresoras"');
                            term.echo(' ');
                            usuarios.push(usuario);
                            valida_3.removeClass().text("✓").addClass("txt-success")
                            return false;
                        }
                        else{
                            term.echo('Se creó el grupo '+usuario);
                            usuarios.push(usuario);
                            return false;
                        }

                    }
                    if (comando === "groupdel"){
                        if (!existe(usuarios, usuario)) {
                            term.echo("Error !!! el grupo "+ usuario+ " no existe");
                            return false;
                        }

                        else if (usuario === "grupo_backup") {
                              term.echo(' ');
                            term.echo('Se eliminó el grupo "grupo_backup"');
                            term.echo(' ');
                            usuarios.splice( $.inArray(usuario, usuarios), 1 );
                            valida_4.removeClass().text("✓").addClass("txt-success")
                            return false;
                        }
                        else{
                            term.echo('Se elimino el grupo '+usuario);
                            usuarios.splice( $.inArray(usuario, usuarios), 1 )
                            return false;
                        }

                    }


                }, {
                    greetings: '   ',
                    name: 'js_demo',
                    height: 400,
                    width: $('#div_modelo').width() - 10,
                    prompt: 'Suricata Anonima:~$ '
                });
            });
        });


    </script>
@endsection
