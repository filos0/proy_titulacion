@extends('layouts.blog')

@section('content')


    @if($errors->any())
        @foreach($errors->all() as $error)
            <script>
                swal({
                    title: "{{$errors->all()[1]}}",
                    text: "{{$errors->all()[2]}}",
                    type: "{{$errors->all()[0]}}",
                    showCancelButton: false,
                    confirmButtonColor: "#ff0005",
                    allowOutsideClick: false,
                    allowEscapeKey: false,
                    confirmButtonText: "Aceptar"
                });
            </script>
        @endforeach
    @endif
    <div class="container-fluid pt-15">
        <div class="col-lg-12">
            <div class="panel panel-default card-view panel-refresh relative">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-lg-10 col-md-offset-1">
                                <div style="font-size: 24px">
                                    <div class="text-center">
                                        <span style="color: #007D3C;font-weight: bold">Bienvenido a nuestra aplicación </span>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="text-justify ">
                                          <span>
                                              Esta aplicación permite a los alumnos, que cursen la materia de <b>SOLARIS</b>, tener una herramienta que simule el entorno de la consola de comandos de SOLARIS.
                                          </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="row">
                            <div class="text-center">
                                <img src="{{asset('solaris_logo.png')}}" class="">
                            </div>
                        </div>

                        <div class="row" style="margin-top: 85px">
                            <div class="col-lg-10 col-md-offset-1">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="panel panel-default card-view pa-0 " style="background-color: #007D3C;">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body pa-0">
                                                <div class="sm-data-box">
                                                    <div class="container-fluid">
                                                        <div class="row" style="color: white;">
                                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                                <span class=" block counter">
                                                                    <span class="counter-anim font-24"
                                                                          style="font-weight: bold">
                                                                        {{\App\Models\registro_model::all()->count()}}
                                                                    </span>
                                                                </span>
                                                                <span class="weight-500 uppercase-font block font-13">
                                                                    Practicas <br> Realizadas
                                                                </span>
                                                            </div>
                                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-right">
                                                                <i class="icon-user-following data-right-rep-icon txt-light-grey"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="panel panel-default card-view pa-0" style="background-color: #007D3C;">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body pa-0">
                                                <div class="sm-data-box">
                                                    <div class="container-fluid">
                                                        <div class="row" style="color: white">
                                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                                <span class=" block counter">
                                                                    <span class="counter-anim font-24"
                                                                          style="font-weight: bold">
                                                                         {{\App\Models\practica_model::all()->count()}}
                                                                    </span>
                                                                </span>
                                                                <span class="weight-500 uppercase-font block">Practicas</span>
                                                            </div>
                                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                                <i class="icon-layers data-right-rep-icon txt-light-grey"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="panel panel-default card-view pa-0" style="background-color: #007D3C;">
                                        <div class="panel-wrapper collapse in">
                                            <div class="panel-body pa-0">
                                                <div class="sm-data-box">
                                                    <div class="container-fluid">
                                                        <div class="row" style="color: white">
                                                            <div class="col-xs-6 text-center pl-0 pr-0 data-wrap-left">
                                                                <span class=" block counter">
                                                                    <span class="counter-anim font-24"
                                                                          style="font-weight: bold">
                                                                        @php $total = 0 ; @endphp
                                                                        @foreach(\App\Models\registro_model::all() as $registro)
                                                                            @php $total = $total + $registro->numero_comandos ; @endphp
                                                                        @endforeach
                                                                        {{$total}}
                                                                    </span>
                                                                </span>
                                                                <span class="weight-500 uppercase-font block">Comandos tecleados</span>
                                                            </div>
                                                            <div class="col-xs-6 text-center  pl-0 pr-0 data-wrap-right">
                                                                <i class="fa fa-keyboard-o  data-right-rep-icon txt-light-grey"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
