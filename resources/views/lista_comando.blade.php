@extends('layouts.blog')

@section('content')
    <div class="container-fluid pt-15">
        <div class="col-lg-12">
            <div class="panel panel-default card-view panel-refresh relative">
                <div class="panel-wrapper collapse in">
                    <div class="panel-heading " style="background-color: white;border: white 1px solid">
                        <div class="row">
                            <div class="text-center">
                                <span style="color: #007D3C;font-weight: bold;font-size: 24px">Lista de comandos</span>
                            </div>
                        </div>
                    </div>
                    <div class="panel-body">
                       <div class="col-lg-10 col-lg-offset-1">
                           <div class="table-responsive">
                               <table class="table table-hover">
                                   <thead>
                                   <tr>
                                       <th class="text-center" style="font-weight: bold;font-size: 18px">Nombre</th>
                                       <th class="text-center" style="font-weight: bold;font-size: 18px">Descripción</th>
                                   </tr>
                                   </thead>
                                   <tbody style="font-size: 15px">
                                   @foreach($comandos as $comando)
                                       <tr>
                                           <td class="text-center">{{$comando->nombre}}</td>
                                           <td>{{$comando->descripcion}}</td>
                                       </tr>
                                   @endforeach
                                   </tbody>
                               </table>
                           </div>
                       </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
