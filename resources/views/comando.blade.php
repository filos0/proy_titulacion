@extends('layouts.blog')

@section('content')
    <div class="container-fluid pt-15">
        <div class="col-lg-12">
            <div class="panel panel-default card-view panel-refresh relative">
                <div class="panel-wrapper collapse in">
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-lg-10 col-md-offset-1">
                                <div style="font-size: 24px">
                                    <div class="text-left">
                                        <span style="color: #007D3C;font-weight: bold">Nombre del Comando :</span>
                                        <br>
                                        <span> {{$comando->nombre}}</span>
                                        <br>
                                        <br>
                                    </div>
                                    <div class="text-justify ">
                                        <span style="color: #007D3C;font-weight: bold">Descripción : </span>
                                        <br>
                                        <span>{{$comando->descripcion}}</span>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
