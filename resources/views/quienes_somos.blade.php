@extends('layouts.blog')

@section('content')
    <div class="container-fluid pt-10">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="panel">
                    <div class="panel-heading">
                        <div class="text-center">
                            <span style="color: #007D3C;font-weight: bold;font-size: 24px">Nuestro Equipo</span>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row mt-10">
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="panel  card-view ">
                                    <div class="panel-wrapper ">
                                        <div class="panel-body pa-0">

                                            <div class="mt-15 mb-15">
                                                <div class="text-center">
                                                    <img src="{{asset('perfil.png')}}" class="img-responsive" alt="">
                                                </div>
                                                <div class="text-center">
                                                    <span style="font-weight: bold"
                                                          class="text-center txt-dark font-22">Hernández Contreras Eliezer Abraham</span><br>
                                                    <span class="text-center txt-grey font-18">aka "El Bombon"</span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="text-center">
                                        <span class=" txt-dark font-18"><b>Actividades Realizadas</b></span>
                                        <ul class="mt-5">
                                            <li> -</li>
                                            <li> -</li>
                                            <li> -</li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="panel panel-default card-view ">
                                    <div class="panel-wrapper ">
                                        <div class="panel-body pa-0">

                                            <div class="mt-15 mb-15">
                                                <div class="text-center">
                                                    <img src="{{asset('perfil.png')}}" class="img-responsive" alt="">
                                                </div>
                                                <div class="text-center">
                                                    <span style="font-weight: bold"
                                                          class="text-center txt-dark font-22">Lazcano Lopez Jorge</span><br>
                                                    <span class="text-center txt-grey font-18">aka "El Yorch"</span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="text-center">
                                        <span class=" txt-dark font-18"><b>Actividades Realizadas</b></span>
                                        <ul class="mt-5">
                                            <li> -</li>
                                            <li> -</li>
                                            <li> -</li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="panel panel-default card-view ">
                                    <div class="panel-wrapper ">
                                        <div class="panel-body pa-0">

                                            <div class="mt-15 mb-15">
                                                <div class="text-center">
                                                    <img src="{{asset('perfil.png')}}" class="img-responsive" alt="">
                                                </div>
                                                <div class="text-center">
                                                    <span style="font-weight: bold"
                                                          class="text-center txt-dark font-22">Macías Carrillo Andrés</span><br>
                                                    <span class="text-center txt-grey font-18">aka "El Andy</span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="text-center">
                                        <span class=" txt-dark font-18"><b>Actividades Realizadas</b></span>
                                        <ul class="mt-5">
                                            <li> -</li>
                                            <li> -</li>
                                            <li> -</li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="panel panel-default card-view ">
                                    <div class="panel-wrapper ">
                                        <div class="panel-body pa-0">

                                            <div class="mt-15 mb-15">
                                                <div class="text-center">
                                                    <img src="{{asset('perfil.png')}}" class="img-responsive" alt="">
                                                </div>
                                                <div class="text-center">
                                                    <span style="font-weight: bold"
                                                          class="text-center txt-dark font-22">Rocha Villanueva Jonathan</span><br>
                                                    <span class="text-center txt-grey font-18">aka "El Rey"</span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <div class="text-center">
                                        <span class=" txt-dark font-18"><b>Actividades Realizadas</b></span>
                                        <ul class="mt-5">
                                            <li> -</li>
                                            <li> -</li>
                                            <li> -</li>
                                        </ul>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
@endsection
