@extends('layouts.blog')

@section('content')
    <div class="container-fluid pt-25">
        <!-- Row -->
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-5 col-xs-12">
                <div class="panel panel-default card-view pt-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box bg-white">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                                <span class="txt-dark block counter">$<span
                                                            class="counter-anim">15,678</span></span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">growth</span><i
                                                        class="zmdi zmdi-caret-down txt-danger font-21 ml-5 vertical-align-middle"></i></span>
                                        </div>
                                        <div class="col-xs-6 text-left  pl-0 pr-0 pt-25 data-wrap-right">
                                            <div id="sparkline_4"
                                                 style="width: 100px; overflow: hidden; margin: 0px auto;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default card-view pt-0">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body pa-0">
                            <div class="sm-data-box bg-white">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-xs-6 text-left pl-0 pr-0 data-wrap-left">
                                                <span class="txt-dark block counter"><span
                                                            class="counter-anim">46.41</span>%</span>
                                            <span class="block"><span
                                                        class="weight-500 uppercase-font txt-grey font-13">population</span><i
                                                        class="zmdi zmdi-caret-up txt-success font-21 ml-5 vertical-align-middle"></i></span>
                                        </div>
                                        <div class="col-xs-6 text-left  pl-0 pr-0 pt-25 data-wrap-right">
                                            <div id="sparkline_5"
                                                 style="width: 100px; overflow: hidden; margin: 0px auto;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default card-view">
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body sm-data-box-1">
                            <span class="uppercase-font weight-500 font-14 block text-center txt-dark">success index this year</span>
                            <div class="cus-sat-stat weight-500 txt-primary text-center mt-5">
                                <span class="counter-anim">65</span><span>%</span>
                            </div>
                            <div class="progress-anim mt-20">
                                <div class="progress">
                                    <div class="progress-bar progress-bar-primary wow animated progress-animated"
                                         role="progressbar" aria-valuenow="65" aria-valuemin="0"
                                         aria-valuemax="100"></div>
                                </div>
                            </div>
                            <ul class="flex-stat mt-5">
                                <li class="half-width">
                                    <span class="block">joined shcool</span>
                                    <span class="block txt-dark weight-500 font-15">
												<i class="zmdi zmdi-trending-up txt-success font-20 mr-10"></i>52
											</span>
                                </li>
                                <li class="half-width">
                                    <span class="block">dropped shcool</span>
                                    <span class="block txt-dark weight-500 font-15">+14.29</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-7 col-xs-12">
                <div class="panel panel-default card-view panel-refresh relative">
                    <div class="refresh-container">
                        <div class="la-anim-1"></div>
                    </div>
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Employment rate</h6>
                        </div>
                        <div class="pull-right">
                            <a href="#" class="pull-left inline-block refresh mr-15">
                                <i class="zmdi zmdi-replay"></i>
                            </a>
                            <div class="pull-left inline-block dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
                                   role="button"><i class="zmdi zmdi-more-vert"></i></a>
                                <ul class="dropdown-menu bullet dropdown-menu-right" role="menu">
                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i
                                                    class="icon wb-reply" aria-hidden="true"></i>option 1</a></li>
                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i
                                                    class="icon wb-share" aria-hidden="true"></i>option 2</a></li>
                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i
                                                    class="icon wb-trash" aria-hidden="true"></i>option 3</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div id="e_chart_2" class="" style="height:349px;"></div>
                            <div class="label-chatrs text-center">
                                <div class="inline-block mr-15">
                                    <span class="clabels inline-block bg-blue mr-5"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">developer</span>
                                </div>
                                <div class="inline-block mr-15">
                                    <span class="clabels inline-block bg-purple mr-5"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">designer</span>
                                </div>
                                <div class="inline-block">
                                    <span class="clabels inline-block bg-skyblue mr-5"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">manager</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12">
                <div class="panel panel-default card-view panel-refresh relative">
                    <div class="refresh-container">
                        <div class="la-anim-1"></div>
                    </div>
                    <div class="panel-heading">
                        <div class="pull-left">
                            <h6 class="panel-title txt-dark">Education Growth</h6>
                        </div>
                        <div class="pull-right">
                            <a href="#" class="pull-left inline-block refresh mr-15">
                                <i class="zmdi zmdi-replay"></i>
                            </a>
                            <div class="pull-left inline-block dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false"
                                   role="button"><i class="zmdi zmdi-more-vert"></i></a>
                                <ul class="dropdown-menu bullet dropdown-menu-right" role="menu">
                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i
                                                    class="icon wb-reply" aria-hidden="true"></i>option 1</a></li>
                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i
                                                    class="icon wb-share" aria-hidden="true"></i>option 2</a></li>
                                    <li role="presentation"><a href="javascript:void(0)" role="menuitem"><i
                                                    class="icon wb-trash" aria-hidden="true"></i>option 3</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-wrapper collapse in">
                        <div class="panel-body">
                            <div id="e_chart_3" class="" style="height:348px;"></div>
                            <div class="label-chatrs">
                                <div class="inline-block mr-15">
                                    <span class="clabels inline-block bg-primary mr-5"></span>
                                    <span class="clabels-text font-12 inline-block txt-dark capitalize-font">Higher education</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
