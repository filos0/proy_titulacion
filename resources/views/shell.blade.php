@extends('layouts.blog')

@section('content')
    <div class="pt-25">
        <!-- Row -->
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="sm-data-box bg-white">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="text-center">
                                        <span class="txt-dark block counter">1.- ¿Comó creamos una carpeta/directorio en la terminal?</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="table-responsive" id="div_modelo">
                    <div id="term_demo"></div>
                </div>
            </div>


            <div class="col-md-6">
                <div class="panel panel-default">
                    <div class="panel-body ">
                       <span>

                       </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            $(function ($,) {
                $('#term_demo').terminal(function (command, term) {

                        if (command === "ls") {
                            term.echo('carpeta 1');
                            term.echo('carpeta 2');
                            term.echo('carpeta 3');
                        }

                }, {
                    greetings: '   ',
                    name: 'js_demo',
                    height: 400,
                    width: $('#div_modelo').width(),
                    prompt: 'Anonimo:~$ '
                });
            });
        });


    </script>
@endsection
