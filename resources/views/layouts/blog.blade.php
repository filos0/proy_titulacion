<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Proyecto De Titulación</title>
    <!-- Favicon -->
    <link href="{{asset('vendors/bower_components/datatables/media/css/jquery.dataTables.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css')}}" rel="stylesheet"
          type="text/css">
    <link href="{{asset('vendors/bower_components/bootstrap-select/dist/css/bootstrap-select.min.css')}}"
          rel="stylesheet" type="text/css"/>
    <link href="{{asset('dist/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('css/terminal.min.css')}}" rel="stylesheet"/>
    <link href="{{asset('css/sweetalert2.css')}}" rel="stylesheet"/>
    <script type="text/javascript" src="{{asset('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/sweetalert2.js')}}"></script>

</head>

<body>
<!-- /Preloader -->
<div class="wrapper  theme-2-active pimary-color-blue">
    <!-- Top Menu Items -->
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="mobile-only-brand pull-left">
            <div class="nav-header pull-left">
                <div class="logo-wrap">
                    <div class="text-center">
                        <a href="#">

                            <span class="brand-text">SOLARIS</span>
                        </a>
                    </div>
                </div>
            </div>
            <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);">
                <i class="zmdi zmdi-menu"></i>
            </a>
            <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);">
                <i class="zmdi zmdi-more"></i>
            </a>

        </div>
        <div id="mobile_only_nav" class="mobile-only-nav pull-right">
            <ul class="nav navbar-right top-nav pull-right">
            </ul>
        </div>
    </nav>
    <!-- /Top Menu Items -->

    <!-- Left Sidebar Menu -->

    <div class="fixed-sidebar-left">
        <ul class="nav navbar-nav side-nav nicescroll-bar">
            <li class="navigation-header">
                <div class="text-center">
                    <div class="col-lg-10 col-lg-offset-1">
                        <br>
                        <img class=" img-rounded img-responsive" src="{{asset('ipn.png')}}" alt="">
                        <br>
                    </div>
                    <div class="clearfix">
                        <div id="text-carousel" class="carousel slide" data-interval="2000" data-ride="carousel">
                            <div class="row">
                                <div class="col-xs-12">
                                    <p style="font-weight: bold">SOY POLITÉCNICO</p>
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <div class="carousel-content">
                                                <div>
                                                    <p>Porque aspiro a ser todo un hombre</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="carousel-content">
                                                <div>
                                                    <p>Porque exijo mis deberes antes que mis derechos</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="carousel-content">
                                                <div>
                                                    <p>Por convicción y no por circunstancia</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="carousel-content">
                                                <div>
                                                    <p>Para alcanzar las conquistas universales y ofrecerlas a mi
                                                        pueblo</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="carousel-content">
                                                <div>
                                                    <p>Porque me duele la Patria en mis entrañas y aspiro a calmar sus
                                                        dolencias</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="item">
                                            <div class="carousel-content">
                                                <div>
                                                    <p>Porque ardo en deseos de despertar al hermano dormido</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <br>
                </div>
            </li>
            <li class="navigation-header">
                <span>Menú Principal</span>
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a href="{{url("/Introduccion")}}" data-toggle="collapse" data-target="#ecom_dr">
                    <div class="pull-left">
                        <i class="mr-20 ti-book"></i>
                        <span class="right-nav-text">Introducción</span>
                    </div>
                    <div class="clearfix"></div>
                </a>
            </li>
            <li>
                <a href="{{url("/Lista/Comandos")}}" data-toggle="collapse" data-target="#ecom_dr">
                    <div class="pull-left">
                        <i class="mr-20  glyphicon glyphicon-bookmark"></i>
                        <span class="right-nav-text">Lista Comandos</span>
                    </div>
                    <div class="clearfix"></div>
                </a>

            </li>
            <li>
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#ui_dr">
                    <div class="pull-left">
                        <i class="fa fa-book mr-20"></i>
                        <span class="right-nav-text">Practicas</span>
                    </div>
                    <div class="pull-right">
                        <i class="zmdi zmdi-caret-down"></i>
                    </div>
                    <div class="clearfix"></div>
                </a>
                <ul id="ui_dr" class="collapse collapse-level-1 two-col-list">

                    <li>
                        <a href="{{url('/Practica/1')}}">Práctica 1</a>
                    </li>
                    <li>
                        <a href="{{url('/Practica/2')}}">Práctica 2</a>
                    </li>
                    <li>
                        <a href="{{url('/Practica/3')}}">Práctica 3</a>
                    </li>
                    <li>
                        <a href="{{url('/Practica/4')}}">Práctica 4</a>
                    </li>
                    <li>
                        <a href="{{url('/Practica/5')}}">Práctica 5</a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="{{url("/Quienes_Somos")}}" data-toggle="collapse" data-target="#ecom_dr">
                    <div class="pull-left">
                        <i class="mr-20  glyphicon glyphicon-user"></i>
                        <span class="right-nav-text">¿Quienes Somos?</span>
                    </div>
                    <div class="clearfix"></div>
                </a>

            </li>
        </ul>
    </div>

    <!-- /Left Sidebar Menu -->


    <!-- Main Content -->
    <div class="page-wrapper">
        @yield('content')

    </div>
    <!-- /Main Content -->

</div>
<!-- /#wrapper -->

<!-- JavaScript -->


<script type="text/javascript" src="{{asset('vendors/bower_components/jquery/dist/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendors/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('vendors/bower_components/datatables/media/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/terminal.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dist/js/jquery.slimscroll.js')}}"></script>
<script type="text/javascript" src="{{asset('vendors/bower_components/moment/min/moment.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('vendors/bower_components/simpleWeather/jquery.simpleWeather.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dist/js/simpleweather-data.js')}}"></script>
<script type="text/javascript"
        src="{{asset('vendors/bower_components/waypoints/lib/jquery.waypoints.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/sweetalert2.all.js')}}"></script>
<script type="text/javascript" src="{{asset('js/sweetalert2.js')}}"></script>
<script type="text/javascript"
        src="{{asset('vendors/bower_components/jquery.counterup/jquery.counterup.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dist/js/dropdown-bootstrap-extended.js')}}"></script>
<script type="text/javascript"
        src="{{asset('vendors/bower_components/owl.carousel/dist/owl.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendors/bower_components/echarts/dist/echarts-en.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendors/echarts-liquidfill.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('vendors/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js')}}"></script>
<script type="text/javascript" src="{{asset('vendors/bower_components/switchery/dist/switchery.min.js')}}"></script>
<script type="text/javascript"
        src="{{asset('vendors/bower_components/bootstrap-select/dist/js/bootstrap-select.min.js')}}"></script>
<script type="text/javascript" src="{{asset('dist/js/init.js')}}"></script>

</body>

</html>
