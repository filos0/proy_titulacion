<?php

namespace App\Http\Controllers;

use App\Models\comando_model as Comando;
use Illuminate\Http\Request;

class ComandosController extends Controller
{
    public function info_comando($comando)
    {
        $comando = Comando::where("nombre", $comando)->get()->last();
        if($comando == null)
            return view('introduccion')->withErrors(array('error', 'ERROR', 'EL comando no lo tenemos en nuestro sistema '));
            else
        return view('comando')
            ->with('comando', $comando);
    }
    public function lista_comandos()
    {
        $comandos = Comando::all();
        return view('lista_comando')
            ->with('comandos', $comandos);
    }
}
