<?php

namespace App\Http\Controllers;

use App\Models\practica_model as Practica;
use App\Models\practica_comando_model as Practica_Comando;
use App\Models\registro_model as Registro;
use Illuminate\Http\Request;

class PracticasController extends Controller
{
    public function practica_1(Request $request)
    {
        $practica = Practica::where('id_pratica', 1)->get()->first();
        return view('practicas.practica_1')
            ->with("practica", $practica);
    }
    public function practica_2(Request $request)
    {
        $practica = Practica::where('id_pratica', 2)->get()->first();
        return view('practicas.practica_2')
            ->with("practica", $practica);
    }
    public function practica_3(Request $request)
    {
        $practica = Practica::where('id_pratica', 3)->get()->first();
        return view('practicas.practica_3')
            ->with("practica", $practica);
    }
    public function practica_4(Request $request)
    {
        $practica = Practica::where('id_pratica', 4)->get()->first();
        return view('practicas.practica_4')
            ->with("practica", $practica);
    }
    public function practica_5(Request $request)
    {
        $practica = Practica::where('id_pratica', 2)->get()->first();
        return view('practicas.practica_5')
            ->with("practica", $practica);
    }
    public function finalizar_practica(Request $request){

        $Registro = new Registro;
        $Registro->practica_id = $request->id_practica;
        $Registro->tiempo = $request->segundos;
        $Registro->numero_comandos = $request->num_comandos;
        $Registro->fecha = date('Y-m-d h:i:s');
        $Registro->ip_visitante = request()->ip();
        $Registro->save();
        return view('introduccion')->withErrors(array('success', 'Practica Finalizada Correctamente', ''));
    }
}
