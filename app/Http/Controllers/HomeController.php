<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function introduccion(Request $request)
    {
       return view('introduccion');
    }
    public function quienes_Somos(Request $request)
    {
        return view('quienes_somos');
    }
}
