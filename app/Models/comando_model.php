<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class comando_model extends Model
{
    protected $table = "comando";
    public $primaryKey = "id_comando";
    public $fillable = ["nombre","descripcion"];
    public $timestamps = false;

}
