<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class practica_model extends Model
{
    protected $table = 'pratica';
    public $primaryKey = 'id_pratica';
    public $fillable = ['nombre','descripcion'];
    public $timestamps = false;

    public function comandos(){
        return $this->hasMany('App\Models\practica_comando_model','practica_id');
    }
}
