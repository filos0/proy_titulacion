<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class registro_model extends Model
{
    protected $table = 'registro';
    public $primaryKey = 'id_registro';
    public $fillable = ['practica_id','tiempo','numero_comandos','fecha','ip_visitante'];
    public $timestamps = false;

}
