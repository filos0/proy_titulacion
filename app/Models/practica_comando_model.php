<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class practica_comando_model extends Model
{
    protected $table = 'practica_comando';
    public $primaryKey = 'id_practica_comando';
    public $fillable = ['practica_id','comando_id'];
    public $timestamps = false;


    public function descripcion(){
        return $this->belongsTo('App\Models\comando_model','comando_id');
    }

}
