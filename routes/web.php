<?php


Route::get('/', 'HomeController@introduccion');
Route::get('/Introduccion', 'HomeController@introduccion');
Route::get('/Quienes_Somos', 'HomeController@quienes_Somos');


Route::get('/Practica/1', 'PracticasController@practica_1');
Route::get('/Practica/2', 'PracticasController@practica_2');
Route::get('/Practica/3', 'PracticasController@practica_3');
Route::get('/Practica/4', 'PracticasController@practica_4');
Route::get('/Practica/5', 'PracticasController@practica_5');

Route::post('/Practica/Finalizar', 'PracticasController@finalizar_practica');


Route::get('/Lista/Comandos', 'ComandosController@lista_comandos');
Route::get('/comando/{comando}', 'ComandosController@info_comando');


/*
Auth::routes();

Route::get('/home',  function () {return view('shell');});
*/